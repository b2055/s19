//3
let getCube = 9 ** 3

//4
console.log(`The cube of 9 is ${getCube}`)

//5
let address = ["Marikina", "Olive", 2, "Concepcion Dos"];

//6
let [city, street, houseNo, barangay] = address

console.log(`I live at ${houseNo} - ${street}, ${barangay}, ${city} City`)

//7
let animal = {
		name: "Nezumi",
		feet: 4,
		furType: "Solid",
		furColor: "Black",
		weight: 4.5,
		howCute: "very cute"
}

//8
let{ name, feet, furType, furColor, weight, howCute} = animal


console.log(`My cat, ${name}, has ${feet} feet and he has a ${furType} - ${furColor} fur. ${name} weighs approx. ${weight}kg and he is ${howCute}`)


//9

let arrayNum = [9, 19, 29, 39, 49, 59]

//10
arrayNum.forEach((num) => console.log(num))

//11
let reduceNumber = arrayNum.reduce((num, num2) => num + num2 ) 

console.log(reduceNumber)

// 12

class Dog {
	constructor (name, age, breed) {
		this.name = name
		this.age = age 
		this.breed = breed
	}
}

//13
let myDog = new Dog("Neko", 5, "Asong Kalye");
console.log(myDog)
